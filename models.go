package main

import "gorm.io/gorm"

type CategoryList struct {
	gorm.Model
	ID       int64  // `json:"id" gorm:"primary_key"`
	Inactive bool   // `json:"inactive"`
	Name     string // `gorm:"type:varchar;not null" json:"name"`
	Sequence int    // `gorm:"type:int;not null" json:"sequence"`
}

type TodoItem struct {
	gorm.Model
	ID             int64  // `gorm:"primary_key"json:"id"`
	Inactive       bool   // `json:"inactive"`
	Name           string // `gorm:"type:varchar;not null" json:"name"`
	Content        string // `gorm:"type:varchar;not null" json:"content"`
	CategoryListId int
	CategoryList   CategoryList
	Completed      bool
	Priority       int // `json:"priority"`
	Sequence       int // `gorm:"type:int;not null" json:"sequence"`
}

type ReSequence struct {
	ParentId int64 `json:"parent_id"`
	RecordId int64 `json:"record_id" binding:"required"`
}

// Sample data
func Populate(db *gorm.DB) {
	db.Create(Categories) // no batch insert
	db.Create(Items)
}

var Categories = []CategoryList{
	{Name: "Cat 1"},
	{Name: "Cat 2"},
	{Name: "Cat 3"},
	{Name: "Cat 4"},
}

var Items = []TodoItem{
	{Name: "Item 1", Content: "THIS IS CONTENT 1", CategoryListId: 1, Sequence: 1},
	{Name: "Item 2", Content: "THIS IS CONTENT 2", CategoryListId: 1, Sequence: 2},
	{Name: "Item 3", Content: "THIS IS CONTENT 3", CategoryListId: 2, Sequence: 3},
	{Name: "Item 4", Content: "THIS IS CONTENT 4", CategoryListId: 2, Sequence: 4},
	{Name: "Item 5", Content: "THIS IS CONTENT 5", CategoryListId: 2, Sequence: 5},
	{Name: "Item 6", Content: "THIS IS CONTENT 6", CategoryListId: 3, Sequence: 6},
	{Name: "Item 7", Content: "THIS IS CONTENT 6", CategoryListId: 3, Sequence: 7},
	{Name: "Item 8", Content: "THIS IS CONTENT 6", CategoryListId: 3, Sequence: 8},
	{Name: "Item 9", Content: "THIS IS CONTENT 6", CategoryListId: 3, Sequence: 9},
	{Name: "Item 10", Content: "THIS IS CONTENT 10", CategoryListId: 3, Sequence: 10},
	{Name: "Item 11", Content: "THIS IS CONTENT 11", CategoryListId: 3, Sequence: 11},
	{Name: "Item 12", Content: "THIS IS CONTENT 12", CategoryListId: 3, Sequence: 12},
	{Name: "Item 13", Content: "THIS IS CONTENT 13", CategoryListId: 3, Sequence: 13},
	{Name: "Item 14", Content: "THIS IS CONTENT 14", CategoryListId: 3, Sequence: 14},
	{Name: "Item 15", Content: "THIS IS CONTENT 15", CategoryListId: 3, Sequence: 15},
	{Name: "Item 16", Content: "THIS IS CONTENT 16", CategoryListId: 3, Sequence: 16},
}
