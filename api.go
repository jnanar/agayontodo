package main

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func create(c *gin.Context) {
	model_name := c.Params.ByName("model")
	// ShouldBindJSON is not working
	// c.Query("Name") --> value is another way to do it with limited amount of parameters
	if model_name == "category_list" {
		var values CategoryList
		if err := c.ShouldBind(&values); err != nil {
			fmt.Println(err)
		}
		_create(c, &values)
		fmt.Println(values)
	} else if model_name == "todo_item" {
		// We need to create the item with the associated list. We make the asumption that the
		// list is created before the item. THis present to use the _create method in both cases
		var values TodoItem
		if err := c.ShouldBind(&values); err != nil {
			fmt.Println(err)
		}
		var cat CategoryList
		DB.First(&cat, values.CategoryListId)
		values.CategoryList = cat
		result := DB.Create(&values)
		if result.Error != nil {
			c.AbortWithStatus(404)
			return
		}
		c.IndentedJSON(http.StatusOK, &values)
	}
}

func read(c *gin.Context) {
	model_name := c.Params.ByName("model")
	rec_id, _ := strconv.Atoi(c.Params.ByName("id"))
	if model_name == "category_list" {
		var values []CategoryList
		_read(c, &values, rec_id)

	} else if model_name == "todo_item" {
		var values []TodoItem
		_read(c, &values, rec_id)

	}

}

func update(c *gin.Context) {
	model_name := c.Params.ByName("model")
	rec_id, _ := strconv.Atoi(c.Params.ByName("id"))
	if model_name == "category_list" {
		var values CategoryList
		if err := c.BindJSON(&values); err != nil {
			fmt.Println(err)
		}
		var res []CategoryList
		_update(c, &res, rec_id, values)
	} else if model_name == "todo_item" {
		var values TodoItem
		if err := c.BindJSON(&values); err != nil {
			fmt.Println(err)
		}
		var res []TodoItem
		_update(c, &res, rec_id, values)
	}
}

func delete(c *gin.Context) {
	table_name := c.Params.ByName("model")
	rec_id, _ := strconv.Atoi(c.Params.ByName("id"))
	// arj todo : use generics
	if table_name == "category_lists" {
		_delete(c, table_name, rec_id)

	} else if table_name == "todo_items" {
		_delete(c, table_name, rec_id)

	}
	c.Status(http.StatusOK)

}

func resequence(c *gin.Context) {
	model_name := c.Params.ByName("model")
	var values ReSequence
	err := c.BindJSON(&values)
	if err != nil {
		c.Error(err).SetType(gin.ErrorTypeBind)
		return
	}
	_resequence(c, model_name, values.ParentId, values.RecordId)
}
