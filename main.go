package main

import (
	"log"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/jimlawless/cfg"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

const (
	Version               = "v0.0-dev"
	configurationFilePath = "agayonTodo/todo.conf"
	LogInfo               = "\t[TODO INFO]\t"
	LogError              = "\t[TODO ERROR]\t"
	LogDebug              = "\t[TODO DEBUG]\t"
)

var (
	DefaultXdgConfigDirs = os.Getenv("XDG_CONFIG_HOME")
	mapConfig            = make(map[string]string)
	SqliteFilemane       = DefaultXdgConfigDirs + "/agayonTodo/todo.db"
	DB                   *gorm.DB
	TrustedProxy         = ""
	Host                 = ""
	ReleaseMode          = false
	BaseUrl              = ""
)

func init() {
	log.Printf("Running TODO API %v", Version)
	if !loadConfigFile() {
		log.Fatal("Failed to load configuration file.")
	}
	TrustedProxy = mapConfig["trusted_proxy"]
	Host = mapConfig["host"]
}

func main() {
	log.Printf("%sRunning", LogInfo)
	db, err := gorm.Open(sqlite.Open(SqliteFilemane), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	db.AutoMigrate(
		CategoryList{},
		TodoItem{})
	if mapConfig["populate"] == "true" {
		Populate(db)
	}
	if mapConfig["release"] == "true" {
		ReleaseMode = true
	}
	baseUrl, success := mapConfig["base_url"]
	if mapConfig["base_url"] == "" && !success {
		baseUrl = "/api"
	}
	BaseUrl = baseUrl
	DB = db
	Run()

}

func Run() {
	if ReleaseMode {
		gin.SetMode(gin.ReleaseMode)
	}
	router := gin.Default()

	if TrustedProxy != "" {
		router.SetTrustedProxies([]string{TrustedProxy})
	}
	router.PUT(BaseUrl+"/create/:model", create)
	router.GET(BaseUrl+"/read/:model/:id", read)
	router.GET(BaseUrl+"/read/:model", read)
	router.POST(BaseUrl+"/update/:model/:id", update)
	router.DELETE(BaseUrl+"/delete/:model/:id", delete)
	router.POST(BaseUrl+"/resequence/:model", resequence)
	router.Run(Host)
}

func loadConfigFile() bool {
	ret := false
	for _, path := range strings.Split(DefaultXdgConfigDirs, ":") {
		log.Println("Try to find configuration file into " + path)
		configFile := path + "/" + configurationFilePath
		if _, err := os.Stat(configFile); err == nil {
			// The config file exist
			err = cfg.Load(configFile, mapConfig)
			if err == nil {
				// And has been loaded succesfully
				log.Println("Find configuration file at " + configFile)
				ret = true
				break
			}
		}
	}
	return ret
}
