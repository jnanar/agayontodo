# AgayonTodo

AgayonTodo is a simple go service providing an API for to-do lists. 

## Dependencies

 * [gorm](https://gorm.io/index.html)
 * [gin-gonic](https://gin-gonic.com/)
 * [cfg](https://github.com/jimlawless/cfg) for the configuration file.

## Configure
Configure the service by editing the ``todo.conf`` file. This configuration file has to be placed following the [XDG specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html) (example ``~/config/agayonTodo/todo.conf``).
An example of the config file can be found in [the repos](https://gitlab.com/jnanar/agayontodo/-/blob/master/todo.conf).

## Help
To get any help, please visit the XMPP conference room at [discuss@chat.agayon.be](xmpp://discuss@chat.agayon.be?join) with your prefered client.
