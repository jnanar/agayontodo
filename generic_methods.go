package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func _create(c *gin.Context, values interface{}) {
	result := DB.Create(values)
	if result.Error != nil {
		c.AbortWithStatus(404)
		return
	}
	c.IndentedJSON(http.StatusOK, values)
}

func _read(c *gin.Context, atts interface{}, id int) {
	var result *gorm.DB
	if id == 0 {
		result = DB.Order("sequence ASC, id").Find(atts)
	} else {
		result = DB.Order("sequence ASC, id").Find(atts, id)
	}
	if result.Error != nil {
		c.AbortWithStatus(404)
		return
	}
	c.IndentedJSON(http.StatusOK, &atts)
}

func _update(c *gin.Context, atts interface{}, id int, values interface{}) {
	result := DB.Find(atts, id)
	// We update all fields thanks to json bind
	DB.Model(&atts).Where("id = ?", id).Select("*").Updates(values)
	if result.Error != nil {
		c.AbortWithStatus(404)
		return
	}
	c.IndentedJSON(http.StatusOK, &atts)
}

func _delete(c *gin.Context, table string, id int) {
	if table == "category_lists" {
		rec := CategoryList{}
		DB.Table(table).Where("id = ?", id).Delete(&rec)
	} else if table == "todo_items" {
		rec := TodoItem{}
		DB.Table(table).Where("id = ?", id).Delete(&rec)
	}
	c.IndentedJSON(http.StatusOK, gin.H{"status": "success"})
}

func _resequence(c *gin.Context, model_name string, parentId int64, recordId int64) {
	// Resequence the items. We move one item on top of another.

	if model_name == "todo_item" {
		var results []TodoItem
		var moved_record TodoItem
		var parent_record TodoItem
		movedRecord := DB.Find(&moved_record, recordId)
		parentRecord := DB.Find(&parent_record, parentId)
		if parentRecord.Error != nil || movedRecord.Error != nil || movedRecord.RowsAffected == 0 {
			c.AbortWithStatus(404)
			return
		}
		// Get all records ordered by sequence. We will put the movedRecord on top of the parentRecord
		// All records with sequence higher than the parentRecord are reorganized. Store all the records inside the result variable
		DB.Order("sequence asc").Where("category_list_id = ? AND id != ? AND sequence > ?", moved_record.CategoryListId, moved_record.ID, parent_record.Sequence).Find(&results)
		seq := parent_record.Sequence + 10
		moved_record.Sequence = seq
		DB.Model(&moved_record).Where("id = ?", moved_record.ID).Updates(&moved_record)
		for idx := range results {
			seq += 10
			DB.Model(&results).Where("id = ?", results[idx].ID).Update("Sequence", seq)
		}
		c.IndentedJSON(http.StatusOK, gin.H{"moved_record": &moved_record, "following_values": &results})
		return
	}
	// ARJ Todo: same for category list without copy paste
	c.IndentedJSON(http.StatusOK, gin.H{"status": "success"})
}
